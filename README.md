# LoRaNet32

ESP32 BLE/WiFi to RN2438 LoRaWAN bridge

## Features
 - USB Host for each MCU
 - solor LiPo battery charge controller
 - ESP <> LORA UART
 - 32MBit shared memory (no connected)

# Previews

Preview  |   Board
---------|--------
![preview](3dboard.png)|![board](board.png)

# User Flow

One of the ideas for maintaining low power during user sessions
is to utilize `Service Workers` and `Web Bluetooth`. The idea is a
user would download a `Service Worker` on first connect. This app
would handle establishing a BLE connection then booting the user
from WiFi.

![userflow](userflow.png)
