EESchema Schematic File Version 2
LIBS:LoRaNeT32-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:rn2483
LIBS:ESP32-footprints-Shem-Lib
LIBS:espressif-xess
LIBS:ft2232
LIBS:CON-SMA-EDGE
LIBS:Winbond
LIBS:CP2102
LIBS:ltc4121-4v2
LIBS:LoRaNeT32-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR022
U 1 1 58E2FDE8
P 4650 5200
F 0 "#PWR022" H 4650 4950 50  0001 C CNN
F 1 "GND" H 4650 5050 50  0000 C CNN
F 2 "" H 4650 5200 50  0001 C CNN
F 3 "" H 4650 5200 50  0001 C CNN
	1    4650 5200
	1    0    0    -1  
$EndComp
Text HLabel 9600 4150 2    60   Input ~ 0
ESP32_IO0
Text HLabel 4650 4850 0    60   Input ~ 0
ESP32_TXD0
Text HLabel 4650 4750 0    60   Input ~ 0
ESP32_RXD0
Text HLabel 6800 3550 2    60   Input ~ 0
ESP32_RXD1
Text HLabel 6800 3650 2    60   Input ~ 0
ESP32_TXD1
$Comp
L ESP32 U5
U 1 1 58E734B0
P 5650 3650
F 0 "U5" H 5050 5350 60  0000 C CNN
F 1 "ESP32" H 6150 5350 60  0000 C CNN
F 2 "ESP32-footprints-Lib:ESP32" H 5650 2025 60  0001 C CNN
F 3 "" H 6100 4550 60  0001 C CNN
	1    5650 3650
	1    0    0    -1  
$EndComp
$Comp
L Crystal_GND23 Y2
U 1 1 58E7377E
P 2850 4450
F 0 "Y2" H 2975 4650 50  0000 L CNN
F 1 "40MHz" V 2700 4500 50  0000 L CNN
F 2 "Crystals:Crystal_SMD_3225-4pin_3.2x2.5mm" H 2850 4450 50  0001 C CNN
F 3 "" H 2850 4450 50  0001 C CNN
F 4 "631-1433-1-ND" H 2850 4450 60  0001 C CNN "digi#"
	1    2850 4450
	0    1    1    0   
$EndComp
$Comp
L Crystal Y3
U 1 1 58E737CB
P 4350 4000
F 0 "Y3" H 4350 4150 50  0000 C CNN
F 1 "32K" H 4350 3850 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_3215-2pin_3.2x1.5mm" H 4350 4000 50  0001 C CNN
F 3 "" H 4350 4000 50  0001 C CNN
	1    4350 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 58E7D3C9
P 2500 4550
F 0 "#PWR023" H 2500 4300 50  0001 C CNN
F 1 "GND" H 2500 4400 50  0000 C CNN
F 2 "" H 2500 4550 50  0001 C CNN
F 3 "" H 2500 4550 50  0001 C CNN
	1    2500 4550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 58E7D3E0
P 3200 4550
F 0 "#PWR024" H 3200 4300 50  0001 C CNN
F 1 "GND" H 3200 4400 50  0000 C CNN
F 2 "" H 3200 4550 50  0001 C CNN
F 3 "" H 3200 4550 50  0001 C CNN
	1    3200 4550
	1    0    0    -1  
$EndComp
$Comp
L C_Small C15
U 1 1 58E7D451
P 2750 4050
F 0 "C15" H 2760 4120 50  0000 L CNN
F 1 "22pF" H 2760 3970 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2750 4050 50  0001 C CNN
F 3 "" H 2750 4050 50  0001 C CNN
	1    2750 4050
	0    1    1    0   
$EndComp
$Comp
L C_Small C18
U 1 1 58E7D539
P 3200 4900
F 0 "C18" H 3210 4970 50  0000 L CNN
F 1 "22pF" H 3210 4820 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3200 4900 50  0001 C CNN
F 3 "" H 3200 4900 50  0001 C CNN
	1    3200 4900
	0    1    1    0   
$EndComp
$Comp
L C_Small C19
U 1 1 58E7D7CD
P 3250 2150
F 0 "C19" H 3260 2220 50  0000 L CNN
F 1 "1uF" H 3260 2070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3250 2150 50  0001 C CNN
F 3 "" H 3250 2150 50  0001 C CNN
	1    3250 2150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C16
U 1 1 58E7D800
P 3000 2150
F 0 "C16" H 3010 2220 50  0000 L CNN
F 1 "100pF" H 3000 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3000 2150 50  0001 C CNN
F 3 "" H 3000 2150 50  0001 C CNN
	1    3000 2150
	1    0    0    -1  
$EndComp
Text GLabel 2700 2050 0    60   Input ~ 0
3V3
$Comp
L GND #PWR025
U 1 1 58E7D9E6
P 2800 2250
F 0 "#PWR025" H 2800 2000 50  0001 C CNN
F 1 "GND" H 2800 2100 50  0000 C CNN
F 2 "" H 2800 2250 50  0001 C CNN
F 3 "" H 2800 2250 50  0001 C CNN
	1    2800 2250
	1    0    0    -1  
$EndComp
$Comp
L C_Small C24
U 1 1 58E7DAFE
P 5300 1550
F 0 "C24" H 5310 1620 50  0000 L CNN
F 1 "0.1uF" H 5300 1700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5300 1550 50  0001 C CNN
F 3 "" H 5300 1550 50  0001 C CNN
	1    5300 1550
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 58E7DB72
P 5300 1700
F 0 "#PWR026" H 5300 1450 50  0001 C CNN
F 1 "GND" H 5300 1550 50  0000 C CNN
F 2 "" H 5300 1700 50  0001 C CNN
F 3 "" H 5300 1700 50  0001 C CNN
	1    5300 1700
	-1   0    0    -1  
$EndComp
Text GLabel 5550 1450 2    60   Input ~ 0
3V3
$Comp
L C_Small C23
U 1 1 58E7DDEE
P 5250 1050
F 0 "C23" H 5260 1120 50  0000 L CNN
F 1 "0.1uF" H 5250 1200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5250 1050 50  0001 C CNN
F 3 "" H 5250 1050 50  0001 C CNN
	1    5250 1050
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C27
U 1 1 58E7DE2C
P 5900 1050
F 0 "C27" H 5910 1120 50  0000 L CNN
F 1 "1uF" H 5900 1200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5900 1050 50  0001 C CNN
F 3 "" H 5900 1050 50  0001 C CNN
	1    5900 1050
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C25
U 1 1 58E7DE81
P 5500 1050
F 0 "C25" H 5510 1120 50  0000 L CNN
F 1 "10uF" H 5500 1200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5500 1050 50  0001 C CNN
F 3 "" H 5500 1050 50  0001 C CNN
	1    5500 1050
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C26
U 1 1 58E7DF70
P 5700 1050
F 0 "C26" H 5710 1120 50  0000 L CNN
F 1 "10uF" H 5700 1200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5700 1050 50  0001 C CNN
F 3 "" H 5700 1050 50  0001 C CNN
	1    5700 1050
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 58E7E0A3
P 5900 1200
F 0 "#PWR027" H 5900 950 50  0001 C CNN
F 1 "GND" H 5900 1050 50  0000 C CNN
F 2 "" H 5900 1200 50  0001 C CNN
F 3 "" H 5900 1200 50  0001 C CNN
	1    5900 1200
	-1   0    0    -1  
$EndComp
Text GLabel 6050 950  2    60   Input ~ 0
3V3
$Comp
L C_Small C17
U 1 1 58E7E4E9
P 3000 3300
F 0 "C17" H 3010 3370 50  0000 L CNN
F 1 "10nF" H 3010 3220 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3000 3300 50  0001 C CNN
F 3 "" H 3000 3300 50  0001 C CNN
	1    3000 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR028
U 1 1 58E7E5D9
P 3000 3450
F 0 "#PWR028" H 3000 3200 50  0001 C CNN
F 1 "GND" H 3000 3300 50  0000 C CNN
F 2 "" H 3000 3450 50  0001 C CNN
F 3 "" H 3000 3450 50  0001 C CNN
	1    3000 3450
	1    0    0    -1  
$EndComp
$Comp
L C_Small C20
U 1 1 58E7E674
P 3500 3400
F 0 "C20" H 3510 3470 50  0000 L CNN
F 1 "3.3nF" H 3510 3320 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3500 3400 50  0001 C CNN
F 3 "" H 3500 3400 50  0001 C CNN
	1    3500 3400
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R12
U 1 1 58E7E6D0
P 3500 3150
F 0 "R12" V 3500 3100 50  0000 L CNN
F 1 "20K" V 3500 2900 50  0000 L CNN
F 2 "Resistors_SMD:R_0402" H 3500 3150 50  0001 C CNN
F 3 "" H 3500 3150 50  0001 C CNN
	1    3500 3150
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C22
U 1 1 58E7EDA7
P 4200 1850
F 0 "C22" H 4210 1920 50  0000 L CNN
F 1 "0.1uF" H 4200 2000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 4200 1850 50  0001 C CNN
F 3 "" H 4200 1850 50  0001 C CNN
	1    4200 1850
	1    0    0    -1  
$EndComp
Text GLabel 3800 1750 0    60   Input ~ 0
3V3
$Comp
L GND #PWR029
U 1 1 58E7EF0C
P 4200 1950
F 0 "#PWR029" H 4200 1700 50  0001 C CNN
F 1 "GND" H 4200 1800 50  0000 C CNN
F 2 "" H 4200 1950 50  0001 C CNN
F 3 "" H 4200 1950 50  0001 C CNN
	1    4200 1950
	1    0    0    -1  
$EndComp
$Comp
L CON-SMA ANT3
U 1 1 58E7F1FD
P 7200 1450
F 0 "ANT3" H 7100 2000 60  0000 C CNN
F 1 "CON-SMA" V 7450 1650 60  0000 C CNN
F 2 "lib:CON-SMA-EDGE" H 7200 1450 60  0001 C CNN
F 3 "" H 7200 1450 60  0000 C CNN
F 4 "CON-SMA-EDGE-S-ND" H 7200 1450 60  0001 C CNN "digi#"
	1    7200 1450
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR030
U 1 1 58E7F41A
P 7600 1700
F 0 "#PWR030" H 7600 1450 50  0001 C CNN
F 1 "GND" H 7600 1550 50  0000 C CNN
F 2 "" H 7600 1700 50  0001 C CNN
F 3 "" H 7600 1700 50  0001 C CNN
	1    7600 1700
	1    0    0    -1  
$EndComp
$Comp
L C_Small C28
U 1 1 58E89BBE
P 7300 2200
F 0 "C28" H 7300 2250 50  0000 L CNN
F 1 "0.1uF" H 7300 2100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 7300 2200 50  0001 C CNN
F 3 "" H 7300 2200 50  0001 C CNN
	1    7300 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 58E89C6E
P 7300 2350
F 0 "#PWR031" H 7300 2100 50  0001 C CNN
F 1 "GND" H 7300 2200 50  0000 C CNN
F 2 "" H 7300 2350 50  0001 C CNN
F 3 "" H 7300 2350 50  0001 C CNN
	1    7300 2350
	1    0    0    -1  
$EndComp
$Comp
L C_Small C21
U 1 1 58E8A952
P 3800 2700
F 0 "C21" H 3810 2770 50  0000 L CNN
F 1 "1uF" H 3810 2620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3800 2700 50  0001 C CNN
F 3 "" H 3800 2700 50  0001 C CNN
	1    3800 2700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR032
U 1 1 58E8A9B5
P 3800 2850
F 0 "#PWR032" H 3800 2600 50  0001 C CNN
F 1 "GND" H 3800 2700 50  0000 C CNN
F 2 "" H 3800 2850 50  0001 C CNN
F 3 "" H 3800 2850 50  0001 C CNN
	1    3800 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR033
U 1 1 58E94E36
P 9000 6200
F 0 "#PWR033" H 9000 5950 50  0001 C CNN
F 1 "GND" H 9000 6050 50  0000 C CNN
F 2 "" H 9000 6200 50  0001 C CNN
F 3 "" H 9000 6200 50  0001 C CNN
	1    9000 6200
	1    0    0    -1  
$EndComp
NoConn ~ 6500 3500
NoConn ~ 6500 3600
NoConn ~ 6500 3700
NoConn ~ 6500 3800
NoConn ~ 6500 3900
NoConn ~ 6500 4000
NoConn ~ 4800 3900
NoConn ~ 4800 3800
NoConn ~ 4800 3400
NoConn ~ 4800 3300
NoConn ~ 4800 3200
NoConn ~ 4800 3100
Text GLabel 7550 2050 2    60   Input ~ 0
3V3
Text GLabel 3500 2600 0    60   Input ~ 0
ESP_VDD_SDIO
Text GLabel 9100 5600 2    60   Input ~ 0
ESP_VDD_SDIO
Text GLabel 8100 3100 0    60   Input ~ 0
3V3
$Comp
L GND #PWR034
U 1 1 5900607E
P 8100 2900
F 0 "#PWR034" H 8100 2650 50  0001 C CNN
F 1 "GND" H 8100 2750 50  0000 C CNN
F 2 "" H 8100 2900 50  0001 C CNN
F 3 "" H 8100 2900 50  0001 C CNN
	1    8100 2900
	0    1    1    0   
$EndComp
$Comp
L GND #PWR035
U 1 1 5900D7B3
P 10150 3550
F 0 "#PWR035" H 10150 3300 50  0001 C CNN
F 1 "GND" H 10150 3400 50  0000 C CNN
F 2 "" H 10150 3550 50  0001 C CNN
F 3 "" H 10150 3550 50  0001 C CNN
	1    10150 3550
	-1   0    0    -1  
$EndComp
NoConn ~ 6500 4200
Text HLabel 9600 4650 2    60   Input ~ 0
ESP32_EN
$Comp
L Q_NPN_BCE Q2
U 1 1 5902C9FA
P 8750 4650
F 0 "Q2" H 8950 4700 50  0000 L CNN
F 1 "Q_NPN_BCE" H 8950 4600 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 8950 4750 50  0001 C CNN
F 3 "" H 8750 4650 50  0001 C CNN
F 4 "MMSS8050-L-TPMSTR-ND" H 8750 4650 60  0001 C CNN "Mfg_Part_No"
	1    8750 4650
	-1   0    0    1   
$EndComp
$Comp
L Q_NPN_BCE Q1
U 1 1 5902CAC5
P 8750 4150
F 0 "Q1" H 8950 4200 50  0000 L CNN
F 1 "Q_NPN_BCE" H 8950 4100 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 8950 4250 50  0001 C CNN
F 3 "" H 8750 4150 50  0001 C CNN
F 4 "MMSS8050-L-TPMSTR-ND" H 8750 4150 60  0001 C CNN "Mfg_Part_No"
	1    8750 4150
	-1   0    0    -1  
$EndComp
Text Notes 9600 4100 0    60   ~ 0
RTS
Text Notes 9600 4600 0    60   ~ 0
DTR
$Comp
L R_Small R10
U 1 1 5902E16A
P 9250 4150
F 0 "R10" H 9280 4170 50  0000 L CNN
F 1 "12k" H 9280 4110 50  0000 L CNN
F 2 "Resistors_SMD:R_0402" H 9250 4150 50  0001 C CNN
F 3 "" H 9250 4150 50  0001 C CNN
F 4 "311-12KJRTR-ND" H 9250 4150 60  0001 C CNN "Mfg_Part_No"
	1    9250 4150
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R11
U 1 1 5902E1E5
P 9250 4650
F 0 "R11" H 9280 4670 50  0000 L CNN
F 1 "12k" H 9280 4610 50  0000 L CNN
F 2 "Resistors_SMD:R_0402" H 9250 4650 50  0001 C CNN
F 3 "" H 9250 4650 50  0001 C CNN
F 4 "311-12KJRTR-ND" H 9250 4650 60  0001 C CNN "Mfg_Part_No"
	1    9250 4650
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C12
U 1 1 5902EBA1
P 9150 5050
F 0 "C12" H 9160 5120 50  0000 L CNN
F 1 "1u" H 9160 4970 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 9150 5050 50  0001 C CNN
F 3 "" H 9150 5050 50  0001 C CNN
	1    9150 5050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 5902EC18
P 9150 5150
F 0 "#PWR036" H 9150 4900 50  0001 C CNN
F 1 "GND" H 9150 5000 50  0000 C CNN
F 2 "" H 9150 5150 50  0001 C CNN
F 3 "" H 9150 5150 50  0001 C CNN
	1    9150 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4800 4750 4650 4750
Wire Wire Line
	4650 4850 4800 4850
Wire Wire Line
	4650 5200 4650 5100
Wire Wire Line
	4650 5100 4800 5100
Wire Wire Line
	4800 4100 4500 4100
Wire Wire Line
	3200 4550 3200 4450
Wire Wire Line
	3200 4450 3050 4450
Wire Wire Line
	2350 4450 2650 4450
Wire Wire Line
	2500 4050 2500 4550
Wire Wire Line
	2650 4050 2500 4050
Connection ~ 2500 4450
Wire Wire Line
	3300 4400 4800 4400
Wire Wire Line
	3300 4050 3300 4400
Wire Wire Line
	2850 4050 3300 4050
Wire Wire Line
	3000 4050 3000 4200
Wire Wire Line
	3000 4200 2850 4200
Wire Wire Line
	2850 4200 2850 4300
Connection ~ 3000 4050
Wire Wire Line
	3550 4500 4800 4500
Wire Wire Line
	3550 4500 3550 4900
Wire Wire Line
	3550 4900 3300 4900
Wire Wire Line
	3100 4900 2350 4900
Wire Wire Line
	2350 4900 2350 4450
Wire Wire Line
	2850 4600 2850 4750
Wire Wire Line
	2850 4750 3550 4750
Connection ~ 3550 4750
Wire Wire Line
	2700 2050 4050 2050
Connection ~ 3250 2050
Connection ~ 3000 2050
Wire Wire Line
	2800 2250 3250 2250
Connection ~ 3000 2250
Connection ~ 3400 2050
Wire Wire Line
	4700 1450 5550 1450
Connection ~ 2750 2050
Connection ~ 5300 1450
Wire Wire Line
	4700 1450 4700 2100
Wire Wire Line
	4700 2100 4800 2100
Wire Wire Line
	4800 2200 4700 2200
Wire Wire Line
	4700 2200 4700 2300
Wire Wire Line
	4700 2300 4800 2300
Wire Wire Line
	4550 950  4550 2250
Wire Wire Line
	4550 950  6050 950 
Connection ~ 4700 2250
Wire Wire Line
	5250 1150 5900 1150
Wire Wire Line
	5900 1150 5900 1200
Connection ~ 5900 950 
Wire Wire Line
	4800 2950 4100 2950
Wire Wire Line
	4100 2950 4100 3050
Wire Wire Line
	4100 3050 3000 3050
Wire Wire Line
	3000 3050 3000 3200
Wire Wire Line
	3000 3450 3000 3400
Wire Wire Line
	3400 3150 3400 3400
Wire Wire Line
	3600 3150 3600 3400
Wire Wire Line
	4800 2850 4150 2850
Wire Wire Line
	4150 2850 4150 3250
Wire Wire Line
	3300 3250 3400 3250
Connection ~ 3400 3250
Wire Wire Line
	4150 3250 3600 3250
Connection ~ 3600 3250
Wire Wire Line
	3300 3250 3300 3050
Connection ~ 3300 3050
Connection ~ 4200 1750
Wire Wire Line
	7450 1300 7600 1300
Wire Wire Line
	7600 1300 7600 1700
Wire Wire Line
	7450 1400 7600 1400
Connection ~ 7600 1400
Wire Wire Line
	7600 1500 7450 1500
Connection ~ 7600 1500
Wire Wire Line
	7450 1600 7600 1600
Connection ~ 7600 1600
Wire Wire Line
	6900 1800 6900 2100
Wire Wire Line
	6900 2100 6500 2100
Wire Wire Line
	7300 2300 7300 2350
Wire Wire Line
	7000 2050 7550 2050
Wire Wire Line
	7000 2050 7000 2400
Wire Wire Line
	7000 2400 6500 2400
Wire Wire Line
	5300 1650 5300 1700
Wire Wire Line
	4550 2250 4700 2250
Wire Wire Line
	3800 1750 4450 1750
Wire Wire Line
	4450 1750 4450 2400
Wire Wire Line
	4050 2050 4050 2500
Wire Wire Line
	4050 2500 4800 2500
Wire Wire Line
	4700 2500 4700 2600
Wire Wire Line
	4700 2600 4800 2600
Connection ~ 4700 2500
Wire Wire Line
	4800 2750 4600 2750
Wire Wire Line
	4600 2750 4600 2600
Wire Wire Line
	4600 2600 3500 2600
Wire Wire Line
	3800 2850 3800 2800
Connection ~ 3800 2600
Wire Wire Line
	4500 4100 4500 4000
Wire Wire Line
	4800 4200 4200 4200
Wire Wire Line
	4200 4200 4200 4000
Connection ~ 5500 1150
Connection ~ 5700 1150
Connection ~ 5500 950 
Connection ~ 5700 950 
Connection ~ 5250 950 
Wire Wire Line
	9000 6200 9000 6100
Wire Wire Line
	9000 6100 8900 6100
Wire Wire Line
	9100 5600 8900 5600
Wire Wire Line
	6500 4900 7400 4900
Wire Wire Line
	7400 4900 7400 5700
Wire Wire Line
	7400 5700 7900 5700
Wire Wire Line
	6500 5000 7350 5000
Wire Wire Line
	7350 5000 7350 5900
Wire Wire Line
	7350 5900 7900 5900
Wire Wire Line
	6500 5100 7300 5100
Wire Wire Line
	7300 5100 7300 5800
Wire Wire Line
	7300 5800 7900 5800
Wire Wire Line
	7500 4800 7500 5600
Wire Wire Line
	7500 5600 7900 5600
Wire Wire Line
	4450 2400 4800 2400
Wire Wire Line
	7300 2050 7300 2100
Connection ~ 7300 2050
Wire Wire Line
	6500 2600 7700 2600
Wire Wire Line
	7700 2600 7700 3000
Wire Wire Line
	7700 3000 8300 3000
Wire Wire Line
	8100 3100 8300 3100
Wire Wire Line
	6500 2700 7550 2700
Wire Wire Line
	7550 2700 7550 2500
Wire Wire Line
	7550 2500 8200 2500
Wire Wire Line
	8200 2500 8200 2600
Wire Wire Line
	8200 2600 8300 2600
Wire Wire Line
	6500 2800 7750 2800
Wire Wire Line
	7750 2800 7750 2700
Wire Wire Line
	7750 2700 8300 2700
Wire Wire Line
	6500 2900 7800 2900
Wire Wire Line
	7800 2900 7800 2800
Wire Wire Line
	7800 2800 8300 2800
Wire Wire Line
	8300 2900 8100 2900
Wire Wire Line
	10150 3550 10150 3500
Wire Wire Line
	10150 3500 10000 3500
Wire Wire Line
	6750 3400 8300 3400
Wire Wire Line
	8300 3500 7450 3500
Wire Wire Line
	8300 3200 6850 3200
Wire Wire Line
	6850 3200 6850 3000
Wire Wire Line
	6850 3000 6500 3000
Wire Wire Line
	8300 3300 6800 3300
Wire Wire Line
	6800 3300 6800 3100
Wire Wire Line
	6800 3100 6500 3100
Wire Wire Line
	6750 3400 6750 3200
Wire Wire Line
	6750 3200 6500 3200
Wire Wire Line
	6500 4100 7450 4100
Wire Wire Line
	7450 4100 7450 3500
Wire Wire Line
	8650 3950 8650 3850
Wire Wire Line
	8650 3850 7800 3850
Wire Wire Line
	7800 3850 7800 4350
Wire Wire Line
	7800 4350 6500 4350
Wire Wire Line
	6500 4450 7800 4450
Wire Wire Line
	7800 4450 7800 4950
Wire Wire Line
	7800 4950 9050 4950
Wire Wire Line
	8650 4950 8650 4850
Wire Wire Line
	9600 4650 9350 4650
Wire Wire Line
	9150 4650 8950 4650
Wire Wire Line
	8950 4150 9150 4150
Wire Wire Line
	9350 4150 9600 4150
Wire Wire Line
	9050 4950 9050 4850
Wire Wire Line
	9050 4850 9150 4850
Wire Wire Line
	9150 4850 9150 4950
Connection ~ 8650 4950
Wire Wire Line
	8650 4350 9450 4350
Wire Wire Line
	9450 4350 9450 4650
Connection ~ 9450 4650
Wire Wire Line
	8650 4450 8900 4450
Wire Wire Line
	9450 4150 9450 4300
Connection ~ 9450 4150
Wire Wire Line
	8900 4450 8900 4300
Wire Wire Line
	8900 4300 9450 4300
Wire Wire Line
	6500 4800 7500 4800
$Comp
L W25Q16JVSNIQ U8
U 1 1 58E94D47
P 8400 5850
F 0 "U8" H 8400 6350 50  0000 C CNN
F 1 "W25Q16JVSNIQ" H 8400 6250 50  0000 C CNN
F 2 "Housings_SOIC:SOIJ-8_5.3x5.3mm_Pitch1.27mm" H 8400 5450 50  0001 C CNN
F 3 "https://www.winbond.com/resource-files/w25q16jv%20spi%20revd%2008122016.pdf" H 8400 5350 50  0001 C CNN
F 4 "W25Q16JVSNIQ" H 8400 5050 50  0001 C CNN "MPN"
F 5 "Winbond" H 8400 5150 50  0001 C CNN "Manuf"
F 6 "Winbond W25Q16JVSNIQ" H 8400 5250 50  0001 C CNN "BOM"
F 7 "W25Q16JVSNIQ" H 8400 5850 60  0001 C CNN "digi#"
	1    8400 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4600 7700 4600
Wire Wire Line
	7900 6000 7700 6000
Wire Wire Line
	7700 6000 7700 4600
Wire Wire Line
	6500 4700 7600 4700
Wire Wire Line
	7600 4700 7600 6100
Wire Wire Line
	7600 6100 7900 6100
Wire Wire Line
	6500 3300 6700 3300
Wire Wire Line
	6700 3300 6700 3550
Wire Wire Line
	6700 3550 6800 3550
Wire Wire Line
	6500 3400 6650 3400
Wire Wire Line
	6650 3400 6650 3650
Wire Wire Line
	6650 3650 6800 3650
$Comp
L Micro_SD_Card_Det J5
U 1 1 5907608E
P 9200 3000
F 0 "J5" H 8550 3700 50  0000 C CNN
F 1 "Micro_SD_Card_Det" H 9850 3700 50  0000 R CNN
F 2 "" H 11250 3700 50  0001 C CNN
F 3 "" H 9200 3100 50  0001 C CNN
	1    9200 3000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
