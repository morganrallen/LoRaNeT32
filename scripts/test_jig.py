#!/usr/bin/env python2

from os import sys
import argparse
import pcbnew
import numpy as np

parser = argparse.ArgumentParser(description='Generate test jig schematic and board from board input')

parser.add_argument('--hole', metavar='h', type=str,
        help='module name to be used to find mounting holes')

parser.add_argument('board', metavar='b', type=str,
        help='Input board')

args = parser.parse_args()

board = pcbnew.LoadBoard(args.board)

layertable = {}

numlayers = pcbnew.LAYER_ID_COUNT
for i in range(numlayers):
    layertable[board.GetLayerName(i)] = i

FSilk = layertable['F.SilkS']

tp_labels = []
mounting_holes = []

print("\nCreating output board")
output = pcbnew.BOARD()

def find_nearest(array,value):
    return np.argmin(np.abs(array - value))

board_width = board.ComputeBoundingBox().GetWidth()
def packpos(pos):
    return pos.y * board_width + pos.x

def unpackpos(pos):
    return pcbnew.wxPoint(pos % board_width, floor(pos / board_width))

for drawing in board.GetDrawings():
    if isinstance(drawing, pcbnew.TEXTE_PCB) and drawing.GetText()[0:3] == "TP_":
        # track TP_ labels
        tp_labels.append(drawing)

        drawing.SetMirrored(False)
        drawing.SetLayer(FSilk)
        output.Add(drawing)

print("Got %s Test Points" % len(tp_labels))

def mortonIndex(x, y):
    print("X: %s" % x)
    print("Y: %s" % y)
    return (x, y)

viaKeys = np.array([])
vias = []

for track in board.GetTracks():
    via = pcbnew.GetFirstVia(track)

    if via is not None:
        hash = packpos(via.GetPosition())

        if len(np.where(np.logical_and(viaKeys >= hash, viaKeys <= hash))[0]) > 0:
            continue

        viaKeys = np.append(viaKeys, hash)
        vias.append(via)
        assert viaKeys.size == len(vias), "viaKeys[] and vias[] must be same length"

print("Got %s Vias from GetFirstVia" % len(vias))

for label in tp_labels:
    pos = label.GetPosition()
    idx = find_nearest(viaKeys, packpos(pos))

    via = vias[idx]

    newvia = pcbnew.VIA(output)
    bottomlayer = pcbnew.LAYER_ID_COUNT
    topID = board.GetLayerID("F.Cu");
    bottomID = board.GetLayerID("B.Cu");
    newvia.SetLayerPair(topID, bottomID)
    newvia.SetPosition(
        pcbnew.wxPoint(
            via.GetPosition().x,
            via.GetPosition().y
        )
    )
    newvia.SetViaType(via.GetViaType())
    newvia.SetWidth(via.GetWidth())

    output.Add(newvia)

    # cleanup arrays
    viaKeys = np.delete(viaKeys, idx)
    vias.remove(vias[idx])
    assert viaKeys.size == len(vias), "viaKeys[] and vias[] must be same length"

bounds = np.full([ 2, 2 ], -1)

if args.hole is not None:
    hole = args.hole

    for module in board.GetModules():
        if(module.GetValue() == hole):
            pos = module.GetPosition()
            rect = module.GetFootprintRect()
            height = rect.GetHeight()
            width = rect.GetWidth()

            if pos.x < bounds[0][0] or bounds[0][0] == -1: bounds[0][0] = pos.x - width
            if pos.y < bounds[0][1] or bounds[0][1] == -1: bounds[0][1] = pos.y - height

            if pos.x > bounds[1][0] or bounds[1][0] == -1: bounds[1][0] = pos.x + width
            if pos.y > bounds[1][1] or bounds[1][1] == -1: bounds[1][1] = pos.y + height

            output.Add(module)

seg = pcbnew.DRAWSEGMENT(output)
seg.SetStart(pcbnew.wxPoint(bounds[0][0], bounds[0][1]))
seg.SetEnd(pcbnew.wxPoint(bounds[0][0], bounds[1][1]))
seg.SetLayer(FSilk)
output.Add(seg)

seg = pcbnew.DRAWSEGMENT(output)
seg.SetStart(pcbnew.wxPoint(bounds[0][0], bounds[1][1]))
seg.SetEnd(pcbnew.wxPoint(bounds[1][0], bounds[1][1]))
seg.SetLayer(FSilk)

output.Add(seg)

seg = pcbnew.DRAWSEGMENT(output)
seg.SetStart(pcbnew.wxPoint(bounds[1][0], bounds[1][1]))
seg.SetEnd(pcbnew.wxPoint(bounds[1][0], bounds[0][1]))
seg.SetLayer(FSilk)

output.Add(seg)

seg = pcbnew.DRAWSEGMENT(output)
seg.SetStart(pcbnew.wxPoint(bounds[1][0], bounds[0][1]))
seg.SetEnd(pcbnew.wxPoint(bounds[0][0], bounds[0][1]))
seg.SetLayer(FSilk)

output.Add(seg)

print(bounds)
#board_edge = board.ComputeBoundingBox(True).getWxRect()
#
#poly = pcbnew.SHAPE_POLY_SET()
#print(board.ConvertBrdLayerToPolygonalContours(FSilk, poly))
#
#for i in range(0, 3):
#    seg = pcbnew.DRAWSEGMENT()
#    seg.SetStart(pcbnew.wxPoint(board_edge[i], board_edge[i % 4]))
#    seg.SetEnd(pcbnew.wxPoint(board_edge[i + 1], board_edge[(i + 1) % 4]))
#
#    output.Add(seg)

print("Saving")

output.Save("/tmp/output.kicad_pcb")
